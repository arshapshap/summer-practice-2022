package com.example.task1

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import java.lang.Math.*

class MainActivity : AppCompatActivity() {
    private var inputName : EditText? = null
    private var inputHeight : EditText? = null
    private var inputWeight : EditText? = null
    private var inputAge : EditText? = null

    private var inputErrors : Map<EditText?, Int>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inputName = findViewById(R.id.inputName)
        inputHeight = findViewById(R.id.inputHeight)
        inputWeight = findViewById(R.id.inputWeight)
        inputAge = findViewById(R.id.inputAge)

        inputErrors = mapOf(
            inputName to R.id.inputNameError,
            inputHeight to R.id.inputHeightError,
            inputWeight to R.id.inputWeightError,
            inputAge to R.id.inputAgeError,
        )

        inputName?.doAfterTextChanged {
            setColor(inputName, true)
        }
        inputHeight?.doAfterTextChanged {
            setColor(inputHeight, true)
        }
        inputWeight?.doAfterTextChanged {
            setColor(inputWeight, true)
        }
        inputAge?.doAfterTextChanged {
            setColor(inputAge, true)
        }
    }

    fun onClickCalculate(view: View){
        findViewById<TextView>(R.id.resultTextView)?.text = checkValues()
    }

    private fun checkValues() : String {
        var isCorrect : Boolean = true

        var name = inputName?.text.toString()
        if ((name.length == 0) || (name.length > 50)) {
            setColor(inputName, false)
            isCorrect = false
        }

        var height = inputHeight?.text.toString().toIntOrNull() ?: 0
        if ((height <= 0) || (height >= 250)) {
            setColor(inputHeight, false)
            isCorrect = false
        }

        var weight = inputWeight?.text.toString().toDoubleOrNull() ?: 0.0
        if ((weight <= 0) || (weight >= 250)){
            setColor(inputWeight, false)
            isCorrect = false
        }

        var age = inputAge?.text.toString().toIntOrNull() ?: 0
        if ((age <= 0) || (age >= 150)){
            setColor(inputAge, false)
            isCorrect = false
        }

        if (isCorrect)
            return "$name, Ваш результат: ${calculate(name, height, weight, age)}%!"
        return "Данные введены некорректно"
    }

    private fun setColor(editText: EditText?, isCorrect: Boolean){
        if (isCorrect) {
            editText?.setTextColor(Color.BLACK)
            editText?.setHintTextColor(Color.GRAY)
            findViewById<TextView>(inputErrors?.get(editText) ?: 0).visibility = View.INVISIBLE
        }
        else {
            editText?.setTextColor(Color.parseColor("#FF4040"))
            editText?.setHintTextColor(Color.parseColor("#FF8080"))
            findViewById<TextView>(inputErrors?.get(editText) ?: 0).visibility = View.VISIBLE
        }
    }

    private fun calculate(name: String, height: Int, weight: Double, age: Int) : Double {
        var result : Double = name.length * 5.0
        var bodyMassIndex = 10000 * weight / (height * height)
        result += abs(25.0 - bodyMassIndex) * 10
        result += abs(30 - age) * 3
        return round(min(100 * result / 760, 100.0) * 10) / 10.0
    }
}